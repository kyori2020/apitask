<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return Task::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        // Define las reglas de validación
        $rules = [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'completed' => 'boolean',
        ];
        // Realiza la validación
        $validatedData = $request->validate($rules);

        // Crea la nueva tarea
        $task = Task::create($validatedData);

        // Retorna la respuesta
        return response()->json($task, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
       
        try {
            // Busca la tarea por su ID
            $task = Task::findOrFail($id);

            // Retorna la tarea encontrada
            return response()->json($task, 200);
        } catch (ModelNotFoundException $e) {
            // Retorna una respuesta de tarea no encontrada
            return response()->json(['message' => 'Tarea no encontrada'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        try {
            // Busca la tarea por su ID
            $task = Task::findOrFail($id);
            
            // Actualiza la tarea con los datos del request
            $task->update($request->all());
            // Retorna la tarea actualizada
            return response()->json($task, 200);
        } catch (ModelNotFoundException $e) {
            // Retorna un mensaje de error si la tarea no se encuentra
            return response()->json(['message' => 'No se puede actualizar la tarea. Tarea no encontrada'], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            // Busca la tarea por su ID y la elimina
            Task::findOrFail($id)->delete();
            
            // Retorna un mensaje de éxito
            return response()->json(['message' => 'Tarea eliminada correctamente'], 200);
        } catch (ModelNotFoundException $e) {
            // Retorna un mensaje de error si la tarea no se encuentra
            return response()->json(['message' => 'No se puede eliminar la tarea. Tarea no encontrada'], 404);
        }
    }
}
